# oob: Out of bounds panic avoidance in go
Package oob experiments with ways to avoid array out of bounds panics.

See docs at http://godoc.org/gitlab.com/stu-b-doo/oob
